package com.duchan.mobile.kakao_sample.common.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.duchan.mobile.kakao_sample.domain.main.view.MainView;

/**
 * Created by Busy on 2018. 10. 24..
 */

public abstract class BaseMvpActivity<VIEW extends MvpView, PRESENTER extends MvpPresenter<VIEW>> extends AppCompatActivity
        implements MvpView {
    private PRESENTER presenter;

    protected abstract PRESENTER createPresenter();
    protected abstract void onPreAttached();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onPreAttached();
        presenter = createPresenter();
        presenter.attachView(getMvpView());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    public PRESENTER getPresenter() {
        if (presenter == null) {
            presenter = createPresenter();
        }
        return presenter;
    }

    private VIEW getMvpView() {
        return (VIEW) this;
    }
}
