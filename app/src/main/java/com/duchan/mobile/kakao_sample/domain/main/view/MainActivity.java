package com.duchan.mobile.kakao_sample.domain.main.view;

import com.duchan.mobile.kakao_sample.R;
import com.duchan.mobile.kakao_sample.common.view.CommonActivity;
import com.duchan.mobile.kakao_sample.domain.main.presenter.MainPresenter;

/**
 * Created by Busy on 2018. 10. 24..
 */

public class MainActivity extends CommonActivity<MainView, MainPresenter> implements MainView {

    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter();
    }

    @Override
    protected void initLifeCycle() {

    }

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }
}
