package com.duchan.mobile.kakao_sample.common.mvp;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;

import java.lang.ref.WeakReference;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

/**
 * Created by Busy on 2018. 10. 24..
 */

public abstract class BaseMvpPresenter<VIEW extends MvpView, MODEL> implements MvpPresenter<VIEW> {
    private MODEL model;

    @Nullable
    private WeakReference<VIEW> view;

    @Nullable
    private VIEW nullView;

    protected abstract MODEL createModel();

    public BaseMvpPresenter() {
        this.model = createModel();
    }

    @Override
    public void attachView(VIEW view) {
        this.view = new WeakReference(view);
    }

    @Override
    public void detachView() {
        if (view != null) {
            view.clear();
            view = null;
        }
    }

    @NonNull
    protected MODEL model() {
        return model;
    }

    @UiThread
    @NonNull
    protected VIEW view() {
        if (view != null) {
            VIEW realView = view.get();
            if (realView != null) {
                return realView;
            }
        }

        return getNullView();
    }

    @NonNull
    private VIEW getNullView() {
        if (nullView == null) {
            try {
                Class<VIEW> viewClass = null;
                Class<?> currentClass = getClass();

                while (viewClass == null) {
                    Type genericSuperType = currentClass.getGenericSuperclass();
                    while (!(genericSuperType instanceof ParameterizedType)) {
                        currentClass = currentClass.getSuperclass();
                        genericSuperType = currentClass.getGenericSuperclass();
                    }

                    Type[] types = ((ParameterizedType) genericSuperType).getActualTypeArguments();
                    for (Type type : types) {
                        Class<?> genericType = (Class<?>) type;
                        if (genericType.isInterface() && isSubTypeOfMvpView(genericType)) {
                            viewClass = (Class<VIEW>) genericType;
                            break;
                        }
                    }

                    currentClass = currentClass.getSuperclass();
                }
                nullView = NoOp.of(viewClass);
            } catch (Throwable t) {
                throw new IllegalArgumentException(
                        "The generic type <VIEW extends MvpView> must be the first generic type argument of class "
                                + getClass().getSimpleName()
                                + " (per convention). Otherwise we can't determine which type of View this"
                                + " Presenter coordinates.", t);
            }
        }
        return nullView;
    }

    private boolean isSubTypeOfMvpView(@Nullable Class<?> klass) {
        if (klass == null) {
            return false;
        }
        if (klass.equals(MvpView.class)) {
            return true;
        }
        Class[] superInterfaces = klass.getInterfaces();
        for (int i = 0; i < superInterfaces.length; i++) {
            if (isSubTypeOfMvpView(superInterfaces[0])) {
                return true;
            }
        }
        return false;
    }

}
