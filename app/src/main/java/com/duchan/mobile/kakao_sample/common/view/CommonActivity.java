package com.duchan.mobile.kakao_sample.common.view;

import android.arch.lifecycle.LifecycleObserver;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.duchan.mobile.kakao_sample.common.mvp.BaseMvpActivity;
import com.duchan.mobile.kakao_sample.common.mvp.MvpPresenter;
import com.duchan.mobile.kakao_sample.common.mvp.MvpView;

import butterknife.ButterKnife;

/**
 * Created by Busy on 2018. 10. 24..
 */

public abstract class CommonActivity<VIEW extends MvpView, PRESENTER extends MvpPresenter<VIEW>> extends BaseMvpActivity<VIEW, PRESENTER> {
    protected abstract void initLifeCycle();
    protected abstract int getContentView();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initLifeCycle();
        onPreActivityCreated();
    }

    protected void addLifecycleObserver(@NonNull LifecycleObserver lifecycleObserver) {
        getLifecycle().addObserver(lifecycleObserver);
    }

    @SuppressWarnings("unchecked")
    protected void removeLifecycleObserver(@NonNull LifecycleObserver lifecycleObserver) {
        getLifecycle().removeObserver(lifecycleObserver);
    }

    protected void onPreActivityCreated() {
        // Hooking method
    }

    @Override
    protected void onPreAttached() {
        setContentView(getContentView());
        ButterKnife.bind(this);
    }
}
