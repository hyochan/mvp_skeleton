package com.duchan.mobile.kakao_sample.common.mvp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by Busy on 2018. 10. 24..
 */

public abstract class BaseMvpFragment<VIEW extends MvpView, PRESENTER extends MvpPresenter<VIEW>> extends Fragment
        implements MvpView {

    private PRESENTER presenter;

    protected abstract PRESENTER createPresenter();
    protected abstract void onPreAttached();


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onPreAttached();
        presenter = createPresenter();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter.attachView(getMvpView());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    public PRESENTER getPresenter() {
        if (presenter == null) {
            presenter = createPresenter();
        }
        return presenter;
    }

    private VIEW getMvpView() {
        return (VIEW) this;
    }
}
