package com.duchan.mobile.kakao_sample.domain.main.presenter;

import com.duchan.mobile.kakao_sample.common.mvp.BaseMvpPresenter;
import com.duchan.mobile.kakao_sample.domain.main.model.MainModel;
import com.duchan.mobile.kakao_sample.domain.main.view.MainView;

/**
 * Created by Busy on 2018. 10. 24..
 */

public class MainPresenter extends BaseMvpPresenter<MainView, MainModel> {

    @Override
    protected MainModel createModel() {
        return new MainModel();
    }
}
