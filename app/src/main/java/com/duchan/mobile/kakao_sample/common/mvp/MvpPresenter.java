package com.duchan.mobile.kakao_sample.common.mvp;

/**
 * Created by Busy on 2018. 10. 24..
 */

public interface MvpPresenter<VIEW extends MvpView> {

    void attachView(VIEW view);

    void detachView();
}
