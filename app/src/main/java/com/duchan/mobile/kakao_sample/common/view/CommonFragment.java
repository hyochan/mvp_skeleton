package com.duchan.mobile.kakao_sample.common.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.duchan.mobile.kakao_sample.common.mvp.BaseMvpFragment;
import com.duchan.mobile.kakao_sample.common.mvp.MvpPresenter;
import com.duchan.mobile.kakao_sample.common.mvp.MvpView;

import butterknife.ButterKnife;

/**
 * Created by Busy on 2018. 10. 25..
 */

public abstract class CommonFragment<VIEW extends MvpView, PRESENTER extends MvpPresenter<VIEW>> extends BaseMvpFragment<VIEW, PRESENTER> {
    protected abstract int initLayoutResorce();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(initLayoutResorce(), container, false);
        ButterKnife.bind(this, view);
        return view;
    }
}
